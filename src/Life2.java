import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Hashtable;

import javax.swing.BorderFactory;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;

import CelAre.Arena;

public class Life2 extends JApplet {
	private static final long serialVersionUID = 6100074238419372485L;

	private MainWindow mainWin;

	public static void main(String args[]) {
		// Life2 life = new Life2();
		// return;
	}

	public void init() {
		mainWin = new MainWindow();
		mainWin.start();
	}

	// public Life2() {
	// mainWin = new MainWindow();
	// mainWin.start();
	//
	// return;
	// }

}

class MainWindow extends Thread {
	// janela principal
	JFrame window;

	Arena arena;

	private static final int WINHEIGTH = 300;

	private static final int WINWIDTH = 200;

	private JPanel panel1 = new JPanel();

	private JPanel panel2 = new JPanel();

	private JTextField txtLin = new JTextField(2);

	private JTextField txtCol = new JTextField(2);

	private JButton jogar = new JButton("Jogar");

	private JButton montar = new JButton("Montar");

	private JButton sair = new JButton("Sair");

	private Component lastArenaComp;

	private PlayThread pt;

	JSlider slider = new JSlider(10, 1000, 950);

	public void run() {
		this.paint();
		return;
	}

	// desenha a janela principal
	public void paint() {
		// janela principal
		window = new JFrame("Life");

		window.setSize(WINWIDTH, WINHEIGTH);

		// layout da janela principal
		window.getContentPane().setLayout(new BorderLayout());

		sair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
				return;
			}
		});

		JLabel lblLin = new JLabel("Linhas:");
		JLabel lblCol = new JLabel("Colunas:");

		jogar.setEnabled(false);

		// framesPerSecond.addChangeListener(new SliderListener());
		slider
				.setMajorTickSpacing((slider.getMaximum() - slider.getMinimum()) / 50);
		slider.setPaintTicks(true);

		// Create the label table
		Hashtable labelTable = new Hashtable();
		labelTable.put(new Integer(slider.getMinimum()), new JLabel("Lento"));
		labelTable.put(new Integer(slider.getMaximum()), new JLabel("Rapido"));

		slider.setLabelTable(labelTable);

		slider.setPaintLabels(true);
		slider.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 10));

		slider.addMouseListener(new MouseListener() {
			public void mousePressed(MouseEvent e) {
				try {
					pt.setDelay(slider.getMaximum() - slider.getValue());
				} catch (NullPointerException c) {
				}
			}

			public void mouseExited(MouseEvent e) {
				return;
			}

			public void mouseEntered(MouseEvent e) {
				return;
			}

			public void mouseReleased(MouseEvent e) {
				try {
					pt.setDelay(slider.getMaximum() - slider.getValue());
				} catch (NullPointerException c) {
				}
			}

			public void mouseClicked(MouseEvent e) {
				try {
					pt.setDelay(slider.getMaximum() - slider.getValue());
				} catch (NullPointerException c) {
				}
			}
		});

		montar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int l, c;
				try {
					l = Integer.parseInt(txtLin.getText());
				} catch (NumberFormatException n) {
					txtLin.setText("0");
					return;
				}

				try {
					c = Integer.parseInt(txtCol.getText());
				} catch (NumberFormatException n) {
					txtCol.setText("0");
					return;
				}

				if (c <= 0 || l <= 0) {
					jogar.setEnabled(false);
					return;
				}

				arena = new Arena(l, c);

				try {
					window.getContentPane().remove(lastArenaComp);
				} catch (NullPointerException f) {
				}
				;

				lastArenaComp = arena.paint();

				window.getContentPane().add(lastArenaComp, "Center");

				arena.populate();

				int minH, minW;

				if (arena.getWidthPixels() < WINWIDTH)
					minW = WINWIDTH;
				else
					minW = arena.getWidthPixels();

				if ((slider.getHeight() + panel1.getHeight() + arena
						.getHeightPixels()) < WINHEIGTH)
					minH = WINHEIGTH;
				else
					minH = (slider.getHeight() + panel1.getHeight() + arena
							.getHeightPixels());

				window.setSize(minW, minH);
				window.validate();
				window.paintComponents(window.getGraphics());
				window.pack();
				jogar.setEnabled(true);

				return;
			}
		});

		jogar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (jogar.getText() == "Jogar") {
					txtLin.setEnabled(false);
					txtCol.setEnabled(false);

					jogar.setText("Parar");

					montar.setEnabled(false);

					jogar();

				} else if (jogar.getText() == "Parar") {
					txtLin.setEnabled(true);
					txtCol.setEnabled(true);

					jogar.setEnabled(false);

					montar.setEnabled(true);

					jogar.setText("Jogar");

					parar();

				}
				return;
			}
		});

		panel2.setLayout(new FlowLayout());
		panel2.add(lblLin);
		panel2.add(txtLin);
		panel2.add(lblCol);
		panel2.add(txtCol);

		panel1.setLayout(new GridLayout(4, 1));
		panel1.add(panel2);
		panel1.add(montar);
		panel1.add(jogar);
		panel1.add(sair);
		window.getContentPane().add(panel1, "North");
		window.getContentPane().add(slider, "South");

		// window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		window.setVisible(true);
		window.pack();

	}

	private void jogar() {
		window.paintComponents(window.getGraphics());
		pt = new PlayThread(arena, slider.getMaximum() - slider.getValue(),
				jogar);
		pt.setDaemon(true);
		pt.start();

		return;
	}

	private void parar() {
		// pt.destroy();
		pt.interrupt();
		window.validate();
		window.paintComponents(window.getGraphics());
		window.pack();
		return;
	}

}

class PlayThread extends Thread {
	private Arena arena;

	private long delay;

	private JButton controlButton;

	public PlayThread(Arena arena, int delay, JButton controlButton) {
		this.arena = arena;
		this.delay = delay;
		this.controlButton = controlButton;
	}

	protected void setDelay(long delay) {
		this.delay = delay;
	}

	public void run() {
		while (!arena.isEmpty()) {
			try {
				sleep(delay);
				yield();
			} catch (InterruptedException e) {
				System.out.println("Interrompido");
				break;
			}
			arena.refresh();
		}

		// Gera um click no botao de controle
		controlButton.doClick();
		return;
	}
}
