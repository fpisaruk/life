package CelAre;

public class Apresentador {
	public static void printAll(Arena arena) {
		Posicao pos = new Posicao(0, 0);
		Dimensoes dim;

		dim = arena.getDimensions();

		for (int i = 1; i <= dim.dimY; i++) {
			// pula uma linha
			System.out.println("");
			pos.y = i;
			for (int j = 1; j <= dim.dimX; j++) {
				pos.x = j;
				if (arena.isCelAlive(pos))
					System.out.print("*");
				else
					System.out.print(".");
			}
		}
		System.out.println("");
	}
}
