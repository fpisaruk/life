package CelAre;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JToggleButton;
import javax.swing.border.Border;

public class Celula {

	public static final int DIAGSUPDIR = 1;

	public static final int DIAGINFDIR = 2;

	public static final int DIAGSUPESQ = 3;

	public static final int DIAGINFESQ = 4;

	public static final int CIMA = 5;

	public static final int BAIXO = 6;

	public static final int DIREITA = 7;

	public static final int ESQUERDA = 8;

	private static final int celSize = 15;

	private static boolean turbomodeon = false;

	private boolean alive = false;

	private boolean changeState = false;

	private JToggleButton celButton;

	private static final ImageIcon celViva;

	private static final ImageIcon celMorta;

	static {
		byte viva[] = new byte[640];
		byte morta[] = new byte[549];
		//String sv = null;
		//String sm = null;
		try {
			Celula.class.getResourceAsStream("/imagens/viva.jpg").read(viva);
			Celula.class.getResourceAsStream("/imagens/morta.jpg").read(morta);
			//sv = new String(viva);
			//sm = new String(morta);
		} catch (Exception e) {
			System.out.println(e);
		}
		celViva = new ImageIcon(viva);
		celMorta = new ImageIcon(morta);
	}

	protected static int getSize() {
		return celSize;
	}

	public Celula(boolean alive) {
		this.alive = alive;
	}

	public boolean synchronize() {
		if (changeState) {
			alive = !alive;
			changeState = false;

			celButton.setSelected(alive);

			celButton.update(celButton.getGraphics());

			return true;
		} else
			return false;
	}

	public boolean isAlive() {
		return this.alive;
	}

	public Component paint() {
		celButton = new JToggleButton();

		celButton.setIcon(celMorta);
		celButton.setSelectedIcon(celViva);
		celButton.setSize(celSize, celSize);
		celButton.setBackground(Color.white);
		celButton.setContentAreaFilled(false);
		celButton.setForeground(Color.white);

		Border empty = BorderFactory.createEmptyBorder();
		celButton.setBorder(empty);

		celButton.addMouseListener(new MouseListener() {
			public void mousePressed(MouseEvent e) {
				turbomodeon = true;
				alive = !alive;
				celButton.setSelected(alive);
				return;
			}

			public void mouseExited(MouseEvent e) {
				celButton.setSelected(alive);
				return;
			}

			public void mouseEntered(MouseEvent e) {
				if (turbomodeon) {
					alive = !alive;
					celButton.setSelected(alive);
				}
				return;
			}

			public void mouseReleased(MouseEvent e) {
				turbomodeon = false;
				celButton.setSelected(alive);
				return;
			}

			public void mouseClicked(MouseEvent e) {

			}
		});

		// });

		return celButton;
	}

	// Recebe uma array com as celulas vizinhas.
	public void refresh(Celula[] Neighbors) {
		int cont = 0;

		for (int i = 0; i < Neighbors.length; i++)
			if (Neighbors[i].isAlive())
				cont++;

		// Regras para mudanca de estado.
		if (((alive && cont != 2 && cont != 3) || (!alive && cont == 3)))
			changeState = true;

		return;

	}
}
