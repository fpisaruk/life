package CelAre;

import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;

import javax.swing.JPanel;

public class Arena {

	private boolean empty = true;

	private Dimensoes dimensoes = new Dimensoes(0, 0);

	private JPanel panArena;

	private int heightPix;

	private int widthPix;

	Celula celulas[][];

	// Desenha a arena
	public Container paint() {
		panArena = new JPanel();
		panArena.setLayout(new GridLayout(dimensoes.dimY, dimensoes.dimX));
		panArena.setBackground(Color.white);
		return panArena;
	}

	// Apaga todas as celulas da arena
	public void finalize() {
		panArena.removeAll();
		return;
	}

	protected boolean isCelAlive(Posicao pos) {
		return celulas[pos.y][pos.x].isAlive();
	}

	public boolean isEmpty() {
		return empty;
	}

	public Dimensoes getDimensions() {
		return this.dimensoes;
	}

	public int getHeightPixels() {
		return heightPix;
	}

	public int getWidthPixels() {
		return widthPix;
	}

	public Arena(int linhas, int colunas) {
		dimensoes.dimY = linhas;
		dimensoes.dimX = colunas;
	}

	// desenha as celulas na arena
	public void populate() {
		celulas = new Celula[dimensoes.dimY + 2][dimensoes.dimX + 2];

		// Criamos uma matriz com bordas
		for (int i = 0; i < dimensoes.dimY + 2; i++)
			for (int j = 0; j < dimensoes.dimX + 2; j++)
				celulas[i][j] = new Celula(false);

		for (int i = 1; i <= dimensoes.dimY; i++)
			for (int j = 1; j <= dimensoes.dimX; j++)
				panArena.add(celulas[i][j].paint());

		heightPix = Celula.getSize() * dimensoes.dimY;
		widthPix = Celula.getSize() * dimensoes.dimX;

		panArena.paintComponents(panArena.getGraphics());

		empty = !(dimensoes.dimX > 0 && dimensoes.dimY > 0);

		return;
	}

	public void refresh() {
		int i, j;

		for (i = 1; i <= dimensoes.dimY; i++) {
			for (j = 1; j <= dimensoes.dimX; j++) {
				Celula cels[] = new Celula[8];
				cels[0] = celulas[i + 1][j + 1];
				cels[1] = celulas[i + 1][j];
				cels[2] = celulas[i + 1][j - 1];
				cels[3] = celulas[i][j + 1];
				cels[4] = celulas[i][j - 1];
				cels[5] = celulas[i - 1][j - 1];
				cels[6] = celulas[i - 1][j];
				cels[7] = celulas[i - 1][j + 1];

				celulas[i][j].refresh(cels);
			}
		}

		empty = true;

		for (i = 1; i <= this.dimensoes.dimY; i++)
			for (j = 1; j <= this.dimensoes.dimX; j++)
				empty = !celulas[i][j].synchronize() && empty;

		return;
	}
}
